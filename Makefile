
PKI_NAME ?= system
PKI_SUBJ ?= "/O=Systems-Lab/CN=systems-device"
OVMF_CODE ?= /usr/share/edk2/x64/OVMF_CODE.4m.fd
OVMF_VARS_NAME ?= OVMF_VARS.4m.fd
OUTPUT_OVMF_VARS_NAME ?= ovmf_vars.fd

EXTRA_DIR ?= mkosi.extra

OUTPUT_DIR ?= mkosi.output
AB_RAW_IMAGE_NAME ?= ab_image.raw
AB_IMAGE_NAME ?= ab_image.qcow2
BUNDLE_IMAGE_NAME ?= bundle_image.raw
TEST_PAYLOAD ?= ../caterpillar/target/release/caterpillar

all: build

build-base:
	mkdir -vp "$(OUTPUT_DIR)"
	openssl \
		req \
		-x509 \
		-newkey rsa:4096 \
		-nodes \
		-keyout "$(OUTPUT_DIR)/$(PKI_NAME).key" \
		-out "ab_image/mkosi.extra/etc/rauc/$(PKI_NAME).pem" \
		-subj "$(PKI_SUBJ)"
	cd base_image; mkosi --force build; cd ..

build-single:
	cd single_image; mkosi --force build; cd ..

build-ab:
	cd ab_image; mkosi --force build; cd ..
	qemu-img convert -c -f raw -O qcow2 -- "$(OUTPUT_DIR)/$(AB_RAW_IMAGE_NAME)" "$(OUTPUT_DIR)/$(AB_IMAGE_NAME)"
	qemu-img snapshot -c base "$(OUTPUT_DIR)/$(AB_IMAGE_NAME)"
	./scripts/create_bootloader_entries.sh
	rm -v -- "$(OUTPUT_DIR)/$(AB_RAW_IMAGE_NAME)"

build-bundles:
	./scripts/create_update_disks.sh \
		$(OUTPUT_DIR)/ab_image.root-x86-64_a.raw \
		$(OUTPUT_DIR)/ab_image.esp_a.raw

build: build-base build-single build-ab build-bundles

qemu:
	qemu-system-x86_64 \
		-boot order=d,menu=on,reboot-timeout=5000 \
		-m "size=3072" \
		-machine type=q35,smm=on,accel=kvm,usb=on \
		-drive "if=pflash,format=raw,unit=0,file=$(OVMF_CODE),read-only=on" \
		-drive "file=$(OUTPUT_DIR)/$(OUTPUT_OVMF_VARS_NAME),format=raw,if=pflash,readonly=off,unit=1" \
		-drive "format=qcow2,file=$(OUTPUT_DIR)/$(AB_IMAGE_NAME)" \
		-nographic \
		-nodefaults \
		-chardev "stdio,mux=on,id=console,signal=off" \
		-serial "chardev:console" \
		-mon console

qemu-visual:
	qemu-system-x86_64 \
		-boot order=d,menu=on,reboot-timeout=5000 \
		-m "size=3072" \
		-machine type=q35,smm=on,accel=kvm,usb=on \
		-drive "if=pflash,format=raw,unit=0,file=$(OVMF_CODE),read-only=on" \
		-drive "file=$(OUTPUT_DIR)/$(OUTPUT_OVMF_VARS_NAME),format=raw,if=pflash,readonly=off,unit=1" \
		-drive "format=qcow2,file=$(OUTPUT_DIR)/$(AB_IMAGE_NAME)"

prepare-test:
	cp -v -- $(OUTPUT_DIR)/$(OUTPUT_OVMF_VARS_NAME) $(OUTPUT_DIR)/$(OUTPUT_OVMF_VARS_NAME).test-successful
	qemu-img snapshot -a base $(OUTPUT_DIR)/$(AB_IMAGE_NAME)
	mkdir -p $(OUTPUT_DIR)/writable_mount
	guestmount -a $(OUTPUT_DIR)/$(AB_IMAGE_NAME) -m /dev/sda1 --rw $(OUTPUT_DIR)/writable_mount
	cp -v -- $(TEST_PAYLOAD) $(OUTPUT_DIR)/writable_mount/
	guestunmount $(OUTPUT_DIR)/writable_mount
	sleep 1
	rm -rv -- $(OUTPUT_DIR)/writable_mount

define run_qemu_test
	qemu-system-x86_64 \
		-boot order=d,menu=on,reboot-timeout=5000 \
		-m "size=3072" \
		-machine type=q35,smm=on,accel=kvm,usb=on \
		-smbios "type=11,value=io.systemd.credential:test_environment=$(2)" \
		-drive "if=pflash,format=raw,unit=0,file=$(OVMF_CODE),read-only=on" \
		-drive "file=$(OUTPUT_DIR)/$(OUTPUT_OVMF_VARS_NAME).test-successful,format=raw,if=pflash,readonly=off,unit=1" \
		-drive "format=qcow2,file=$(OUTPUT_DIR)/$(AB_IMAGE_NAME)" \
		-drive "format=qcow2,file=$(1)" \
		-nographic \
		-nodefaults \
		-chardev "stdio,mux=on,id=console,signal=off" \
		-serial "chardev:console" \
		-mon console
endef

define reset_update_disk
	qemu-img snapshot -a base $(1)
endef

define add_update_to_disk
	mkdir -p $(OUTPUT_DIR)/writable_mount
	guestmount -a $(1) -m /dev/sda1 --rw $(OUTPUT_DIR)/writable_mount
	cp -v -- $(2) $(OUTPUT_DIR)/writable_mount/
	guestunmount $(OUTPUT_DIR)/writable_mount
	sleep 1
	rm -rv -- $(OUTPUT_DIR)/writable_mount
endef

define add_override_update_to_disk
	mkdir -p $(OUTPUT_DIR)/writable_mount
	guestmount -a $(1) -m /dev/sda1 --rw $(OUTPUT_DIR)/writable_mount
	mkdir -vp -- $(OUTPUT_DIR)/writable_mount/override
	cp -v -- $(2) $(OUTPUT_DIR)/writable_mount/override
	guestunmount $(OUTPUT_DIR)/writable_mount
	sleep 1
	rm -rv -- $(OUTPUT_DIR)/writable_mount
endef

# btrfs based bundle disk with one valid update bundle
test-success-btrfs-single: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/btrfs_single.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/btrfs_single.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/btrfs_single.qcow2,success_single)

# ext4 based bundle disk with one valid update bundle
test-success-ext4-single: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/ext4_single.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/ext4_single.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/ext4_single.qcow2,success_single)

# vfat based bundle disk with one valid update bundle
test-success-vfat-single: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/vfat_single.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/vfat_single.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/vfat_single.raw,success_single)

# btrfs based bundle disk with two valid update bundles (of differing version)
test-success-btrfs-multiple: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call add_update_to_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/btrfs_multiple.qcow2,success_multiple)

# ext4 based bundle disk with two valid update bundles (of differing version)
test-success-ext4-multiple: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call add_update_to_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/ext4_multiple.qcow2,success_multiple)

# vfat based bundle disk with two valid update bundles (of differing version)
test-success-vfat-multiple: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call add_update_to_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/vfat_multiple.qcow2,success_multiple)

# btrfs based bundle disk with two valid update bundles (of differing version, the lower version in override location)
test-success-btrfs-override: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call add_override_update_to_disk,$(OUTPUT_DIR)/btrfs_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/btrfs_multiple.qcow2,success_override)

# ext4 based bundle disk with two valid update bundles (of differing version, the lower version in override location)
test-success-ext4-override: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call add_override_update_to_disk,$(OUTPUT_DIR)/ext4_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/ext4_multiple.qcow2,success_override)

# vfat based bundle disk with two valid update bundles (of differing version, the lower version in override location)
test-success-vfat-override: prepare-test
	$(call reset_update_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2)
	$(call add_update_to_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2,$(OUTPUT_DIR)/update2.raucb)
	$(call add_override_update_to_disk,$(OUTPUT_DIR)/vfat_multiple.qcow2,$(OUTPUT_DIR)/update.raucb)
	$(call run_qemu_test,$(OUTPUT_DIR)/vfat_multiple.qcow2,success_override)

# btrfs based bundle disk with no update bundle
test-skip-empty-btrfs: prepare-test
	$(call run_qemu_test,$(OUTPUT_DIR)/btrfs_empty.qcow2,skip_empty)

# ext4 based bundle disk with no update bundle
test-skip-empty-ext4: prepare-test
	$(call run_qemu_test,$(OUTPUT_DIR)/ext4_empty.qcow2,skip_empty)

# vfat based bundle disk with no update bundle
test-skip-empty-vfat: prepare-test
	$(call run_qemu_test,$(OUTPUT_DIR)/vfat_empty.qcow2,skip_empty)

.PHONY: all build qemu test-successful
