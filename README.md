# A/B test images

This repository holds tooling to build test images for an A/B setup compatible with RAUC and caterpillar.

## Requirements

* libguestfs
* make
* mkosi
  * systemd-ukify
  * python-pefile
* qemu
* rauc

## Build

To build all images run

```sh
make
```

## Tests

To run tests, first build the `caterpillar` binary.
Afterwards run one of the test scenarios, e.g.:

```sh
TEST_PAYLOAD=../caterpillar/target/release/caterpillar make test-success-btrfs-single
```

If successful, the test should prepare an image with an update bundle, add `caterpillar` to the test system, run the test system using QEMU and eventually shut down the virtual machine.

