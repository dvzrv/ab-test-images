#!/bin/bash

set -eu

readonly _output_dir="${OUTPUTDIR:-mkosi.output}"
output_dir="$(realpath "$_output_dir")"

readonly ab_output_image_name="${AB_OUTPUT_IMAGE_NAME:-ab_image.qcow2}"
readonly ab_output_image="$output_dir/$ab_output_image_name"

readonly ovmf_code="${OVMF_CODE:-/usr/share/edk2/x64/OVMF_CODE.4m.fd}"
readonly ovmf_vars="${OVMF_VARS_NAME:-OVMF_VARS.4m.fd}"
readonly output_ovmf_vars_name="${OUTPUT_OVMF_VARS_NAME:-ovmf_vars.fd}"
readonly output_ovmf_vars="$output_dir/$output_ovmf_vars_name"

readonly single_input_image_name="${SINGLE_INPUT_IMAGE_NAME:-single_image}"
readonly single_input_image="$output_dir/$single_input_image_name"

create_boot_entry_in_ovmf_vars() {
  local qemu_options=(
    -boot "order=d,menu=on,reboot-timeout=5000"
    -m "size=3072"
    -machine "type=q35,smm=on,accel=kvm,usb=on"
    -smbios "type=11,value=io.systemd.credential:set_efi_boot_entries=yes"
    -drive "if=pflash,format=raw,unit=0,file=${ovmf_code},read-only=on"
    -drive "file=$output_ovmf_vars,format=raw,if=pflash,readonly=off,unit=1"
    -drive "format=raw,file=${single_input_image}"
    -drive "format=qcow2,file=${ab_output_image}"
    -nographic
    -nodefaults
    -chardev "stdio,mux=on,id=console,signal=off"
    -serial "chardev:console"
    -mon console
  )

  printf "Copy OVMF vars template\n"
  cp -v -- "/usr/share/edk2/x64/$ovmf_vars" "$output_ovmf_vars"

  printf "Run virtual machine to set UEFI boot entries\n"
  qemu-system-x86_64 "${qemu_options[@]}"
}

create_boot_entry_in_ovmf_vars
