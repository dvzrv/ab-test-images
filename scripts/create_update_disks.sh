#!/bin/bash

set -eu

_rootfs="$(realpath "${1}")"
readonly rootfs="${_rootfs}"

_efi="$(realpath "${2}")"
readonly efi="${_efi}"

_mkosi_dir="$(realpath "${MKOSI_DIR:-bundle_disk}")"
readonly mkosi_dir="$_mkosi_dir"

_output_dir="$(realpath "${OUTPUT_DIR:-mkosi.output}")"
output_dir="$_output_dir"

_extra_dir="$(realpath "${EXTRA_DIR:-ab_image/mkosi.extra}")"
readonly extra_dir="$_extra_dir"
readonly pki_name="${PKI_NAME:-system}"
readonly pki_cert="$extra_dir/etc/rauc/$pki_name.pem"
readonly pki_key="$output_dir/$pki_name.key"

create_update_bundle() {
  local staging_dir="$1"
  local name="$2"
  local version="$3"
  local compatible="$4"
  local output_dir="$5"
  local update_bundle="$output_dir/$name.raucb"

  local esp_name="esp.vfat"
  local root_name="root.img"

  local rauc_bundle_options=(
    --cert="$pki_cert"
    --key="$pki_key"
    "$staging_dir"
    "$update_bundle"
  )

  printf "Create a RAUC update bundle...\n"
  mkdir -vp -- "$staging_dir"
  printf "Copy images to bundle output dir %s...\n" "$staging_dir"
  cp -v -- "$efi" "$staging_dir/$esp_name"
  cp -v -- "$rootfs" "$staging_dir/$root_name"
  printf "Done!\n"

  if [[ -f "$update_bundle" ]]; then
    printf "Update bundle exists. Removing...\n"
    rm -v -- "$update_bundle"
  fi

  printf "Write RAUC manifest...\n"
  {
    printf "[update]\n"
    printf "compatible=%s\n" "$compatible"
    printf "version=%s\n" "$version"
    printf "\n"
    printf "[bundle]\n"
    printf "format=verity\n"
    printf "\n"
    printf "[image.rootfs]\n"
    printf "filename=%s\n" "$root_name"
    printf "\n"
    printf "[image.efi]\n"
    printf "filename=%s\n" "$esp_name"
  } > "$staging_dir/manifest.raucm"
  printf "Done!\n"

  printf "Create update bundle...\n"
  rauc bundle "${rauc_bundle_options[@]}"
  printf "Done!\n"

  rm -frv -- "$staging_dir"
}

create_update_bundle_disk() {
  local name="$1"
  local disk_type="$2"
  local filesystem="$3"
  local previous_pwd="$PWD"

  printf "Create update disk '%s' with %s filesystem...\n" "$name" "$filesystem"
  cd "$mkosi_dir"
  mkosi --force --output "$name" --repart-dir "repart/$filesystem/$disk_type" build
  printf "Done!\n"
  cd "$previous_pwd"
  printf "Clean up extra files...\n"
  rm -frv -- "$mkosi_dir/mkosi.extra"
  qemu-img convert -c -f raw -O qcow2 -- "$output_dir/$name.raw" "$output_dir/$name.qcow2"
  qemu-img snapshot -c base "$output_dir/$name.qcow2"
  rm -fv -- "$output_dir/$name.raw"
}


bundle_staging_dir="$(realpath mkosi.output/bundle_staging)"

create_update_bundle "$bundle_staging_dir" "update" "1.0.0" "system" "$output_dir"
create_update_bundle "$bundle_staging_dir" "update2" "2.0.0" "system" "$output_dir"

create_update_bundle_disk "btrfs_single" "single" "btrfs"
create_update_bundle_disk "ext4_single" "single" "ext4"
create_update_bundle_disk "vfat_single" "single" "vfat"
create_update_bundle_disk "btrfs_multiple" "multiple" "btrfs"
create_update_bundle_disk "ext4_multiple" "multiple" "ext4"
create_update_bundle_disk "vfat_multiple" "multiple" "vfat"
create_update_bundle_disk "btrfs_empty" "empty" "btrfs"
create_update_bundle_disk "ext4_empty" "empty" "ext4"
create_update_bundle_disk "vfat_empty" "empty" "vfat"
